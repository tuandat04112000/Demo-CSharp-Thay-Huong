﻿using System;

namespace da_hinh
{
    class Animal
    {
        public virtual void Speak()
        {
            Console.WriteLine("Dong vat keu ......");
        }
    }
    class Cat : Animal
    {
        public override void Speak()
        {
            Console.WriteLine("Meo keu meo meo");
        }
    }
    class Dog : Animal
    {
        public void Speak()
        {
            Console.WriteLine("Cho keu gau gau");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Animal Cat1 = new Cat();
            Cat1.Speak();

            Animal Dog1 = new Dog();
            Dog1.Speak();

            Console.ReadLine();
        }
    }
}