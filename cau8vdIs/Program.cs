﻿using System;

namespace asitis
{
    public class Student
    { }
    public class Teacher
    { }
    class itisas
    {
        static void testClass(object a)
        {
            if(a is Student)
                { Console.WriteLine($"The instance: {nameof(a)} we recieved as argument is from Student class!"); }
            else if(a is Teacher)
                { Console.WriteLine($"The instance: {nameof(a)} we recieved as argument is from Teacher class!"); }
            else
                { Console.WriteLine($"The instance: {nameof(a)} we recieved as argument is from neither Teacher nor Student class!"); }
        }
        static void Main(string[] args)
        {
            Student s = new Student();
            Teacher t = new Teacher();
            int i = 0;
            testClass(s);
            testClass(t);
            testClass(i);
            Console.ReadKey(); ;
        }
    }
}