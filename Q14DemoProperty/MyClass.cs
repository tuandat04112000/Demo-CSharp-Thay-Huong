namespace DemoProperty
{
    public class MyClass
    {
        
        string message;

        public MyClass(string message){
            this.Message = message;
        }

        public string Message {
            get {

                return this.message;
            }
            set {
                if (value.Length > 0 && value.Length <= 100) {
                    this.message = value;
                }
            }
        }

        public override string ToString(){
            return this.Message;
        }

    }
}