﻿using System;

namespace DemoProperty
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass ms = new MyClass("Hello World!");
            System.Console.WriteLine(ms);

            Cat cat = new Cat();
            cat.Name = "Oggy";
            cat.Age = 2;
            System.Console.WriteLine(cat);
        }
    }
}
