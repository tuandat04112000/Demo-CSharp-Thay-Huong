namespace DemoProperty
{
    abstract class Animal {
 
        // Một property trừu tượng (abstract Property).
        public abstract string Name
        {
            get; set;
        }
 
        // Một property trừu tượng có set & get.
        public abstract int Age
        {
            get;
            set;
        }
    }

    class Cat : Animal {

        string name;
        int age;

        public override string Name {
            get {
                return this.name;
            }
            set {
                this.name = value;
            }
        }

        public override int Age {
            get {
                return this.age;
            }
            set {
                if (value > 0){
                    this.age = value;
                }
            }
        }

        public override string ToString(){
            return $"Cat(name: {this.Name}, age: {this.Age})";
        }

    }

}