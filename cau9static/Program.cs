﻿using System;

namespace cau9static
{
    public static class MyStaticClass
    {
        public static int myStaticVariable = 100;

        public static void MyStaticMethod()
        {
            Console.WriteLine("This is a static method.");
        }

        public static int MyStatrtyicPrope { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
             Console.WriteLine(MyStaticClass.myStaticVariable);
             MyStaticClass.MyStaticMethod();
             MyStaticClass.MyStaticProperty = 100;
             Console.WriteLine(MyStaticClass.MyStaticProperty);
        }
    }
}
